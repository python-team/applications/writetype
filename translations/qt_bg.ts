<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="bg_BG">
<context>
    <name>EspeakInferface</name>
    <message>
        <location filename="espeakInterface.py" line="56"/>
        <source>Feature unavailable</source>
        <translation>Функцията е недостъпна</translation>
    </message>
</context>
<context>
    <name>EspeakInterface</name>
    <message>
        <location filename="espeakInterface.py" line="56"/>
        <source>eSpeak is not installed on this computer.  To use this feature, please install eSpeak or select a new TTS driver in the Settings box.</source>
        <translation>eSpeak не е инсталиран на вашия компютър. За да използвате тази функция, моля инсталирайте eSpeak или изберете нов TTS драйвър от настройките.</translation>
    </message>
</context>
<context>
    <name>FestivalInterface</name>
    <message>
        <location filename="festivalInterface.py" line="60"/>
        <source>Feature unavailable</source>
        <translation>Функцията е недостъпна</translation>
    </message>
    <message>
        <location filename="festivalInterface.py" line="60"/>
        <source>Festival is not installed on this computer.  To use this feature, please install Festival or select a new TTS driver in the Settings box.</source>
        <translation>Festival не е инсталиран на вашия компютър. За да използвате тази функция, моля инсталирайте eSpeak или изберете нов TTS драйвър от настройките.</translation>
    </message>
</context>
<context>
    <name>MainApplication</name>
    <message>
        <location filename="main.py" line="724"/>
        <source>WriteType - </source>
        <translation>WriteType - </translation>
    </message>
    <message>
        <location filename="main.py" line="152"/>
        <source>Untitled Document</source>
        <translation>Неозаглавен документ</translation>
    </message>
    <message>
        <location filename="main.py" line="202"/>
        <source>Crash recovery</source>
        <translation>Възстановяване след срив</translation>
    </message>
    <message>
        <location filename="main.py" line="202"/>
        <source>WriteType found unsaved work from a crash.  Would you like to recover it?</source>
        <translation>WriteType намери незаписан документ вследствие от срив.  Ще желаете ли да го възстановите?</translation>
    </message>
    <message>
        <location filename="main.py" line="206"/>
        <source>Recovered file</source>
        <translation>Възстановен файл</translation>
    </message>
    <message>
        <location filename="main.py" line="226"/>
        <source>Open file</source>
        <translation>Отваряне на файл</translation>
    </message>
    <message>
        <location filename="main.py" line="627"/>
        <source>Save file</source>
        <translation>Записване на файл</translation>
    </message>
    <message>
        <location filename="main.py" line="639"/>
        <source>Save error</source>
        <translation>Записване на грешката</translation>
    </message>
    <message>
        <location filename="main.py" line="300"/>
        <source>WriteType was unable to save your work.  Please check the file extension, ensure that the selected file is writable, and try again.</source>
        <translation>WriteType не успя да запази работата ви. Моля, проверете файловото разширение и дали имате права за запис в директорията.</translation>
    </message>
    <message>
        <location filename="main.py" line="331"/>
        <source>Feature unavailable</source>
        <translation>Функцията е недостъпна</translation>
    </message>
    <message>
        <location filename="main.py" line="331"/>
        <source>The current TTS driver is invalid.  Read-back is unavailable for this session.</source>
        <translation>Текущият TTS драйвър е невалиден.  Гласовият прочит не е достъпен за тази сесия.</translation>
    </message>
    <message>
        <location filename="main.py" line="569"/>
        <source>&lt;i&gt;No suggestion available.&lt;/i&gt;</source>
        <translation>&lt;i&gt;Няма предложения.&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="578"/>
        <source>&lt;i&gt;Diction check completed.&lt;/i&gt;</source>
        <translation>&lt;i&gt;Проверката за грешки приключи.&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="606"/>
        <source>About this program</source>
        <translation>За програмата</translation>
    </message>
    <message>
        <location filename="main.py" line="614"/>
        <source>&lt;html&gt;A new version of WriteType is available!  You are using WriteType version r%2.  Find more information about WriteType version r%3 at: &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;Налична е нова версия на WriteType!  В момента ползвате r%2.  Повече информация за новата версия можете на намерите r%3 на: &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="616"/>
        <source>Your version of WriteType is up to date.  You are using WriteType version r%1.</source>
        <translation>WriteType е обновен до последната версия.  В момента ползвате r%1.</translation>
    </message>
    <message>
        <location filename="main.py" line="618"/>
        <source>There was an unexpected error in establishing a connection.  Please try again later.</source>
        <translation>Възникна грешка при осъществяването на връзка с Интернет. Моля, опитайте по-късно.</translation>
    </message>
    <message>
        <location filename="main.py" line="619"/>
        <source>Updates</source>
        <translation>Обновявания</translation>
    </message>
    <message>
        <location filename="main.py" line="640"/>
        <source>WriteType was unable to save the log file.  Please check the file extension, ensure that the selected file is writable, and try again.</source>
        <translation>WriteType не успя да запази log файл.  Моля, проверете файловото разширение и дали имате права за запис в директорията.</translation>
    </message>
    <message>
        <location filename="main.py" line="650"/>
        <source>Print</source>
        <translation>Принтиране</translation>
    </message>
    <message>
        <location filename="main.py" line="736"/>
        <source>Quit?</source>
        <translation>Излизане?</translation>
    </message>
    <message>
        <location filename="main.py" line="736"/>
        <source>You have unsaved work.  Do you want to save?</source>
        <translation>Имате незаписана работа.  Желаете ли да я запазите?</translation>
    </message>
    <message>
        <location filename="main.py" line="601"/>
        <source>&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;Revision r%1&lt;/span&gt;&lt;/h1&gt;&lt;h2&gt;Copyright 2010 Max Shinn&lt;/h2&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;This software is made available under the GNU General Public License v3 or later. For more information about your rights, see: &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Additional Contributions&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Spanish Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Basque Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Dutch Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:sbasalaev@gmail.com&quot;&gt;Sergey Basalaev&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Russian Translations&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</source>
        <translation type="obsolete">&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;Ревизия r%1&lt;/span&gt;&lt;/h1&gt;&lt;h2&gt;Авторски права 2010 Max Shinn&lt;/h2&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;Тази програма е лицензирана под условията на Общия публичен лиценз на GNU (GPL). За повече информация относно правата ви, посетете: &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Сътрудници&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Превод от испански&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Превод от баски&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Превод от холандски&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:sbasalaev@gmail.com&quot;&gt;Sergey Basalaev&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Превод от руски&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:topgunbg@gmail.com&quot;&gt;Galin Petrov&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Превод от български&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="601"/>
        <source>&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;Revision r%1&lt;/span&gt;&lt;/h1&gt;&lt;h3&gt;Copyright 2010-2012 Max Shinn&lt;/h3&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;This software is made available under the GNU General Public License v3 or later. For more information about your rights, see: &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Additional Contributions&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Spanish Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Basque Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Dutch Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:sbasalaev@gmail.com&quot;&gt;Sergey Basalaev&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Russian Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:topgunbg@gmail.com&quot;&gt;Galin Petrov&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Bulgarian Translations&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</source>
        <translation type="obsolete">&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;Ревизия r%1&lt;/span&gt;&lt;/h1&gt;&lt;h3&gt;Авторски права 2010-2012 Max Shinn&lt;/h3&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;Тази програма е лицензирана под условията на Общия публичен лиценз на GNU (GPL). За повече информация относно правата ви, посетете: &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Сътрудници&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Превод от испански&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Превод от баски&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Превод от холандски&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:sbasalaev@gmail.com&quot;&gt;Sergey Basalaev&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Превод от руски&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:topgunbg@gmail.com&quot;&gt;Galin Petrov&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Превод от български&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="606"/>
        <source>&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;Revision r%1&lt;/span&gt;&lt;/h1&gt;&lt;h3&gt;Copyright 2010-2012 Max Shinn&lt;/h3&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;This software is made available under the GNU General Public License v3 or later. For more information about your rights, see: &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Additional Contributions&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Spanish Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Basque Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Dutch Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:sbasalaev@gmail.com&quot;&gt;Sergey Basalaev&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Russian Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:topgunbg@gmail.com&quot;&gt;Galin Petrov&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Bulgarian Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Agnese Dal Borgo&lt;/td&gt;&lt;td&gt;Italian Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Riccardo Murri&lt;/td&gt;&lt;td&gt;Italian Translations&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</source>
        <translation>&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;Ревизия r%1&lt;/span&gt;&lt;/h1&gt;&lt;h3&gt;Авторски права 2010-2012 Max Shinn&lt;/h3&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;Тази програма е лицензирана под условията на Общия публичен лиценз на GNU (GPL). За повече информация относно правата ви, посетете: &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Сътрудници&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Превод от испански&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Превод от баски&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Превод от холандски&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:sbasalaev@gmail.com&quot;&gt;Sergey Basalaev&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Превод от руски&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:topgunbg@gmail.com&quot;&gt;Galin Petrov&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Превод от български&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Agnese Dal Borgo&lt;/td&gt;&lt;td&gt;Превод от италиански&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Riccardo Murri&lt;/td&gt;&lt;td&gt;Превод от италиански&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="ui_mainwindow.py" line="444"/>
        <source>MainWindow</source>
        <translation>ОсновенПрозорец</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="445"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="446"/>
        <source>Next</source>
        <translation>Напред</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="447"/>
        <source>Go Back </source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="448"/>
        <source>View</source>
        <translation>Изглед</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="449"/>
        <source>Toolbars</source>
        <translation>Ленти с инструменти</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="454"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="451"/>
        <source>Tools</source>
        <translation>Инструменти</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="452"/>
        <source>Help</source>
        <translation>Помощ</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="455"/>
        <source>Edit</source>
        <translation>Редактиране</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="456"/>
        <source>Speak text</source>
        <translation>Прочети текста</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="457"/>
        <source>Save</source>
        <translation>Запазване</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="458"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="459"/>
        <source>Save As...</source>
        <translation>Запазване като...</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="460"/>
        <source>Close</source>
        <translation>Затваряне</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="461"/>
        <source>Open</source>
        <translation>Отваряне</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="462"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="463"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="465"/>
        <source>Bold</source>
        <translation>Удебелено</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="466"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="468"/>
        <source>Italic</source>
        <translation>Наклонено</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="469"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="471"/>
        <source>Underline</source>
        <translation>Подчертано</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="472"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="473"/>
        <source>Speak</source>
        <translation>Прочети</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="474"/>
        <source>Speak Text</source>
        <translation>Прочети текста</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="476"/>
        <source>Undo</source>
        <translation>Отмяна</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="477"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="479"/>
        <source>Redo</source>
        <translation>Възстановяване</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="480"/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="481"/>
        <source>File Toolbar</source>
        <translation>Инструменти за файлове</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="482"/>
        <source>Enable File Toolbar</source>
        <translation>Показване на инструментите за файлове</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="483"/>
        <source>Edit Toolbar</source>
        <translation>Инструменти за редактиране</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="484"/>
        <source>Enable Edit Toolbar</source>
        <translation>Показване на инструментите за редактиране</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="485"/>
        <source>Speaker Toolbar</source>
        <translation>Инструменти за прочит</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="486"/>
        <source>Enable Speaker Toolbar</source>
        <translation>Показване на инструментите за прочит</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="487"/>
        <source>Documentation</source>
        <translation>Документация</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="489"/>
        <source>About WriteType</source>
        <translation>За WriteType</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="490"/>
        <source>About Qt</source>
        <translation>За Qt</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="492"/>
        <source>Print</source>
        <translation>Принтиране</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="493"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="495"/>
        <source>Highlight Mode</source>
        <translation>Режим на осветяване</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="497"/>
        <source>Highlight</source>
        <translation>Осветяване</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="498"/>
        <source>Distraction Free</source>
        <translation>Не се разсейвай</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="499"/>
        <source>Distraction Free Mode</source>
        <translation>Режим &quot;не се разсейвай!&quot;</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="500"/>
        <source>Insert Image</source>
        <translation>Вмъкване на изображение</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="501"/>
        <source>Align Image Left</source>
        <translation>Подравняване на изображението вляво</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="502"/>
        <source>Align Image Right</source>
        <translation>Подравняване на изображението вдясно</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="503"/>
        <source>Image Toolbar</source>
        <translation>Инструменти за изображения</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="504"/>
        <source>Stop</source>
        <translation>Спиране</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="505"/>
        <source>Align Left</source>
        <translation>Подравняване вляво</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="506"/>
        <source>Align Center</source>
        <translation>Подравняване в средата</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="507"/>
        <source>Align Right</source>
        <translation>Подравняване вдясно</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="508"/>
        <source>Double Space</source>
        <translation>Двойно разстояние</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="509"/>
        <source>Single Space</source>
        <translation>Единично разстояние</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="510"/>
        <source>Statistics</source>
        <translation>Статистики</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="511"/>
        <source>Diction Check</source>
        <translation>Проверяване за грешки</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="512"/>
        <source>Cut</source>
        <translation>Изрязване</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="513"/>
        <source>Copy</source>
        <translation>Копиране</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="514"/>
        <source>Paste</source>
        <translation>Поставяне</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="515"/>
        <source>Check for Updates</source>
        <translation>Проверка за обновяване</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="516"/>
        <source>Save Debug Log</source>
        <translation>Запазване на дебъг лога</translation>
    </message>
</context>
<context>
    <name>SettingsDialogBox</name>
    <message>
        <location filename="settings.py" line="67"/>
        <source>Replace:</source>
        <translation>Замяна на:</translation>
    </message>
    <message>
        <location filename="settings.py" line="68"/>
        <source>With:</source>
        <translation>С:</translation>
    </message>
</context>
<context>
    <name>SpellCheckEdit</name>
    <message>
        <location filename="spellCheckEdit.py" line="123"/>
        <source>Spelling:</source>
        <translation>Правопис:</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="132"/>
        <source>Add to dictionary</source>
        <translation>Добавяне към речника</translation>
    </message>
</context>
<context>
    <name>WriteTypeMain</name>
    <message>
        <location filename="main.py" line="687"/>
        <source>Invalid, no sentences found.</source>
        <translation>Невалиден, не са намерени изречения.</translation>
    </message>
    <message>
        <location filename="main.py" line="226"/>
        <source>All Compatible Files (*.wtd *.htm *.html *.txt);;WriteType Document (*.wtd);;Formatted Text (*.html *.htm);;All Files (*.*)</source>
        <translation>Всички поддържани файлове (*.wtd *.htm *.html *.txt);;WriteType документ (*.wtd);;Форматиран текст (*.html *.htm);;Всички файлове (*.*)</translation>
    </message>
    <message>
        <location filename="main.py" line="261"/>
        <source>WriteType Document (*.wtd);;Formatted Text (*.html);;Plain Text (*.txt)</source>
        <translation>WriteType документ (*.wtd);;Форматиран текст (*.html *.htm);;Всички файлове (*.*)</translation>
    </message>
</context>
<context>
    <name>distractionFree</name>
    <message>
        <location filename="ui_distractionfree.py" line="37"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
</context>
<context>
    <name>settingsDialog</name>
    <message>
        <location filename="ui_settings.py" line="231"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="248"/>
        <source>Please enter any custom words you would like to appear in the spell check, one per line.</source>
        <translation>Добавете думи, които искате да се появят при проверката за грешки, по една на ред.</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="249"/>
        <source>Custom Words</source>
        <translation>Новодобавени думи</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="232"/>
        <source>Please select the size of the word completion database:</source>
        <translation>Изберете големината на речниците:</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="238"/>
        <source>Word lists</source>
        <translation>Речник</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="239"/>
        <source>View settings for the custom word completion</source>
        <translation>Настройки за новодобавените думи</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="240"/>
        <source>Minimum letters:</source>
        <translation>Минимален брой букви:</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="241"/>
        <source>Offer phrase completions</source>
        <translation>Довършване на изрази</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="242"/>
        <source>Try to guess misspellings</source>
        <translation>Познаване на правописни грешки</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="243"/>
        <source>Misspelling Settings</source>
        <translation>Настройки за правописните грешки</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="244"/>
        <source>How many entries need to be displayed, at the minimum, before WriteType will attempt to guess the spelling?</source>
        <translation>Колко букви трябва да бъдат въведени минимално преди WriteType да провери за грешка?</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="245"/>
        <source>Misspelling Threshold:</source>
        <translation>Праг на правописните грешки:</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="246"/>
        <source>Advanced Substitutions?</source>
        <translation>Разширено заместване?</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="247"/>
        <source>Word Completion</source>
        <translation>Довършване на думи</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="250"/>
        <source>Use auto-correction</source>
        <translation>Автоматично коригиране</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="251"/>
        <source>Auto-correction settings</source>
        <translation>Настройки на автоматичното коригиране</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="252"/>
        <source>Contractions</source>
        <translation>Съкращения</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="253"/>
        <source>Auto-corrections</source>
        <translation>Автоматично коригиране</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="254"/>
        <source>Default Font:</source>
        <translation>Основен шрифт:</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="255"/>
        <source>Options:</source>
        <translation>Опции:</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="256"/>
        <source>Reading speed:</source>
        <translation>Скорост на четене:</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="257"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="258"/>
        <source>TTS Engine</source>
        <translation>TTS Engine</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="259"/>
        <source>System Default</source>
        <translation>По подразбиране за системата</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="262"/>
        <source>Check document for grammar mistakes</source>
        <translation>Проверка на документа за правописни грешки</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="260"/>
        <source>Use Boring Interface (requires restart)</source>
        <translation>Използване на скучния интерфейс (изисква рестартиране)</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="261"/>
        <source>Read words back as they are typed</source>
        <translation>Прочит на думите като се пишат</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="264"/>
        <source>Other</source>
        <translation>Друго</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="265"/>
        <source>Okay</source>
        <translation>ОК</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="266"/>
        <source>Apply</source>
        <translation>Прилагане</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="267"/>
        <source>Cancel</source>
        <translation>Отказ</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="233"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Bitstream Vera Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;None available&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Bitstream Vera Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Няма налично&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="263"/>
        <source>Check document for spelling mistakes</source>
        <translation>Проверка на документа за правописни грешки</translation>
    </message>
</context>
<context>
    <name>statisticsDialog</name>
    <message>
        <location filename="ui_statistics.py" line="79"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="ui_statistics.py" line="80"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Document Statistics&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Статистики за документа&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_statistics.py" line="85"/>
        <source>Filename: </source>
        <translation>Име на файла:</translation>
    </message>
    <message>
        <location filename="ui_statistics.py" line="86"/>
        <source>Characters: </source>
        <translation>Символи:</translation>
    </message>
    <message>
        <location filename="ui_statistics.py" line="87"/>
        <source>Sentences: </source>
        <translation>Изречения:</translation>
    </message>
    <message>
        <location filename="ui_statistics.py" line="88"/>
        <source>Paragraphs: </source>
        <translation>Абзаци:</translation>
    </message>
    <message>
        <location filename="ui_statistics.py" line="89"/>
        <source>Readability</source>
        <translation>Четимост</translation>
    </message>
    <message>
        <location filename="ui_statistics.py" line="90"/>
        <source>Words: </source>
        <translation>Думи:</translation>
    </message>
</context>
</TS>
