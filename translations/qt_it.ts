<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it">
<context>
    <name>EspeakInferface</name>
    <message>
        <location filename="espeakInterface.py" line="56"/>
        <source>Feature unavailable</source>
        <translation>Funzionalità non disponibile</translation>
    </message>
</context>
<context>
    <name>EspeakInterface</name>
    <message>
        <location filename="espeakInterface.py" line="56"/>
        <source>eSpeak is not installed on this computer.  To use this feature, please install eSpeak or select a new TTS driver in the Settings box.</source>
        <translation>eSpeak non è installato su questo computer.  Per usare questa funzionalità, per favore installa eSpeak oppure seleziona un&apos;altro programma di lettura (TTS driver) nella finestra Impostazioni.</translation>
    </message>
</context>
<context>
    <name>FestivalInterface</name>
    <message>
        <location filename="festivalInterface.py" line="60"/>
        <source>Feature unavailable</source>
        <translation>Funzionalità non disponibile</translation>
    </message>
    <message>
        <location filename="festivalInterface.py" line="60"/>
        <source>Festival is not installed on this computer.  To use this feature, please install Festival or select a new TTS driver in the Settings box.</source>
        <translation>Festival non è installato su questo computer.  Per usare questa funzionalità, per favore installa Festival oppure seleziona un&apos;altro programma di lettura (TTS driver) nella finestra Impostazioni.</translation>
    </message>
</context>
<context>
    <name>MainApplication</name>
    <message>
        <location filename="main.py" line="724"/>
        <source>WriteType - </source>
        <translation>WriteType - </translation>
    </message>
    <message>
        <location filename="main.py" line="152"/>
        <source>Untitled Document</source>
        <translation>Documento senza titolo</translation>
    </message>
    <message>
        <location filename="main.py" line="202"/>
        <source>Crash recovery</source>
        <translation>Ripristino per interruzione inaspettata</translation>
    </message>
    <message>
        <location filename="main.py" line="202"/>
        <source>WriteType found unsaved work from a crash.  Would you like to recover it?</source>
        <translation>Il programma si è interrotto inaspettatamente. Ci sono delle modifiche non salvate.  Vuoi recuperarle?</translation>
    </message>
    <message>
        <location filename="main.py" line="206"/>
        <source>Recovered file</source>
        <translation>File ripristinato</translation>
    </message>
    <message>
        <location filename="main.py" line="226"/>
        <source>Open file</source>
        <translation>Apri file</translation>
    </message>
    <message>
        <location filename="main.py" line="627"/>
        <source>Save file</source>
        <translation>Salva file</translation>
    </message>
    <message>
        <location filename="main.py" line="639"/>
        <source>Save error</source>
        <translation>Errore nel salvataggio</translation>
    </message>
    <message>
        <location filename="main.py" line="300"/>
        <source>WriteType was unable to save your work.  Please check the file extension, ensure that the selected file is writable, and try again.</source>
        <translation>WriteType non è riuscito a salvare il documento.  Controlla l&apos;estensione del file, assicurati che il file selezionato sia scrivibile e prova di nuovo.</translation>
    </message>
    <message>
        <location filename="main.py" line="331"/>
        <source>Feature unavailable</source>
        <translation>Funzionalità non disponibile</translation>
    </message>
    <message>
        <location filename="main.py" line="331"/>
        <source>The current TTS driver is invalid.  Read-back is unavailable for this session.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main.py" line="569"/>
        <source>&lt;i&gt;No suggestion available.&lt;/i&gt;</source>
        <translation>&lt;i&gt;Nessun suggerimento disponibile.&lt;i&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="578"/>
        <source>&lt;i&gt;Diction check completed.&lt;/i&gt;</source>
        <translation>&lt;i&gt;Controllo stilistico completato.&lt;i&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="606"/>
        <source>About this program</source>
        <translation>Informazioni su questo programma</translation>
    </message>
    <message>
        <location filename="main.py" line="606"/>
        <source>&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;Revision r%1&lt;/span&gt;&lt;/h1&gt;&lt;h3&gt;Copyright 2010-2012 Max Shinn&lt;/h3&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;This software is made available under the GNU General Public License v3 or later. For more information about your rights, see: &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Additional Contributions&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Spanish Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Basque Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Dutch Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:sbasalaev@gmail.com&quot;&gt;Sergey Basalaev&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Russian Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:topgunbg@gmail.com&quot;&gt;Galin Petrov&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Bulgarian Translations&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</source>
        <translation type="obsolete">&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;Revisione r%1&lt;/span&gt;&lt;/h1&gt;&lt;h3&gt;Copyright 2010-2012 Max Shinn&lt;/h3&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;Questo software è disponibile secondo i termini della licenza GNU G.P.L. v3 o successiva. Per maggiori informazioni sui tuoi diritti, visita: &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Contributi supplementari&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Traduzione in spagnolo&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Traduzione in basco&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Traduzione in olandese&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:sbasalaev@gmail.com&quot;&gt;Sergey Basalaev&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Traduzione in russo&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:topgunbg@gmail.com&quot;&gt;Galin Petrov&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Traduzione in bulgaro&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="614"/>
        <source>&lt;html&gt;A new version of WriteType is available!  You are using WriteType version r%2.  Find more information about WriteType version r%3 at: &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;È disponibile una nuova versione di WriteType!  Stai usando WriteType versione r%2. Trovi maggiori informazioni su WriteType versione r%3 at: &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="616"/>
        <source>Your version of WriteType is up to date.  You are using WriteType version r%1.</source>
        <translation>La tua versione di WriteType è aggiornata.  Stai usando WriteType versione r%1.</translation>
    </message>
    <message>
        <location filename="main.py" line="618"/>
        <source>There was an unexpected error in establishing a connection.  Please try again later.</source>
        <translation>Si è verificato un errore innatteso nello stabilire una connessione.  Riprova più tardi.</translation>
    </message>
    <message>
        <location filename="main.py" line="619"/>
        <source>Updates</source>
        <translation>Aggiornamenti</translation>
    </message>
    <message>
        <location filename="main.py" line="640"/>
        <source>WriteType was unable to save the log file.  Please check the file extension, ensure that the selected file is writable, and try again.</source>
        <translation>WriteType non è riuscito a salvare il file di registro.  Controlla l&apos;estensione del file, assicurati che il file selezionato sia scrivibile e prova di nuovo.</translation>
    </message>
    <message>
        <location filename="main.py" line="650"/>
        <source>Print</source>
        <translation>Stampa</translation>
    </message>
    <message>
        <location filename="main.py" line="736"/>
        <source>Quit?</source>
        <translation>Uscire?</translation>
    </message>
    <message>
        <location filename="main.py" line="736"/>
        <source>You have unsaved work.  Do you want to save?</source>
        <translation>Ci sono delle modifiche non salvate.  Vuoi salvarle?</translation>
    </message>
    <message>
        <location filename="main.py" line="606"/>
        <source>&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;Revision r%1&lt;/span&gt;&lt;/h1&gt;&lt;h3&gt;Copyright 2010-2012 Max Shinn&lt;/h3&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;This software is made available under the GNU General Public License v3 or later. For more information about your rights, see: &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Additional Contributions&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Spanish Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Basque Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Dutch Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:sbasalaev@gmail.com&quot;&gt;Sergey Basalaev&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Russian Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:topgunbg@gmail.com&quot;&gt;Galin Petrov&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Bulgarian Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Agnese Dal Borgo&lt;/td&gt;&lt;td&gt;Italian Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Riccardo Murri&lt;/td&gt;&lt;td&gt;Italian Translations&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</source>
        <translation>&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;Revisione r%1&lt;/span&gt;&lt;/h1&gt;&lt;h3&gt;Copyright 2010-2012 Max Shinn&lt;/h3&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;Questo software è disponibile secondo i termini della licenza GNU G.P.L. v3 o successiva. Per maggiori informazioni sui tuoi diritti, visita: &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Contributi supplementari&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Traduzione in spagnolo&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Traduzione in basco&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Traduzione in olandese&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:sbasalaev@gmail.com&quot;&gt;Sergey Basalaev&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Traduzione in russo&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:topgunbg@gmail.com&quot;&gt;Galin Petrov&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Traduzione in bulgaro&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Agnese Dal Borgo&lt;/td&gt;&lt;td&gt;Traduzione in italiano&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Riccardo Murri&lt;/td&gt;&lt;td&gt;Traduzione in italiano&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="ui_mainwindow.py" line="444"/>
        <source>MainWindow</source>
        <translation>Finestra principale</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="445"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="446"/>
        <source>Next</source>
        <translation>Avanti</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="447"/>
        <source>Go Back </source>
        <translation>Indietro </translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="448"/>
        <source>View</source>
        <translation>Mostra</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="449"/>
        <source>Toolbars</source>
        <translation>Pulsanti</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="454"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="451"/>
        <source>Tools</source>
        <translation>Strumenti</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="452"/>
        <source>Help</source>
        <translation>Aiuto</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="455"/>
        <source>Edit</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="456"/>
        <source>Speak text</source>
        <translation>Leggi il testo</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="457"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="458"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="459"/>
        <source>Save As...</source>
        <translation>Salva come...</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="460"/>
        <source>Close</source>
        <translation>Chiudi</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="461"/>
        <source>Open</source>
        <translation>Apri</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="462"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="463"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="465"/>
        <source>Bold</source>
        <translation>Grassetto</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="466"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="468"/>
        <source>Italic</source>
        <translation>Italico</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="469"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="471"/>
        <source>Underline</source>
        <translation>Sottolineato</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="472"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="473"/>
        <source>Speak</source>
        <translation>Leggi</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="474"/>
        <source>Speak Text</source>
        <translation>Leggi il testo</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="476"/>
        <source>Undo</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="477"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="479"/>
        <source>Redo</source>
        <translation>Ripeti</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="480"/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="481"/>
        <source>File Toolbar</source>
        <translation>Pulsanti file</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="482"/>
        <source>Enable File Toolbar</source>
        <translation>Abilita i pulsanti dei file</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="483"/>
        <source>Edit Toolbar</source>
        <translation>Pulsanti di modifica</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="484"/>
        <source>Enable Edit Toolbar</source>
        <translation>Abilita i pulsanti di modifica</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="485"/>
        <source>Speaker Toolbar</source>
        <translation>Pulsanti di lettura</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="486"/>
        <source>Enable Speaker Toolbar</source>
        <translation>Abilita i pulsanti di lettura</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="487"/>
        <source>Documentation</source>
        <translation>Documentazione</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="489"/>
        <source>About WriteType</source>
        <translation>Informazioni su WriteType</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="490"/>
        <source>About Qt</source>
        <translation>Informazioni su Qt</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="492"/>
        <source>Print</source>
        <translation>Stampa</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="493"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="495"/>
        <source>Highlight Mode</source>
        <translation>Modalità evidenziatore</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="497"/>
        <source>Highlight</source>
        <translation>Evidenziatore</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="498"/>
        <source>Distraction Free</source>
        <translation>Senza distrazioni</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="499"/>
        <source>Distraction Free Mode</source>
        <translation>Modalità senza distrazioni</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="500"/>
        <source>Insert Image</source>
        <translation>Inserisci un&apos;immagine</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="501"/>
        <source>Align Image Left</source>
        <translation>Allinea immagine a sinistra</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="502"/>
        <source>Align Image Right</source>
        <translation>Allinea immagine a destra</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="503"/>
        <source>Image Toolbar</source>
        <translation>Pulsanti immagine</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="504"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="505"/>
        <source>Align Left</source>
        <translation>Allinea a sinistra</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="506"/>
        <source>Align Center</source>
        <translation>Centra</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="507"/>
        <source>Align Right</source>
        <translation>Allinea a destra</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="508"/>
        <source>Double Space</source>
        <translation>Interlinea doppia</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="509"/>
        <source>Single Space</source>
        <translation>Interlinea singola</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="510"/>
        <source>Statistics</source>
        <translation>Statistiche</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="511"/>
        <source>Diction Check</source>
        <translation>Controllo stilistico</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="512"/>
        <source>Cut</source>
        <translation>Taglia</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="513"/>
        <source>Copy</source>
        <translation>Copia</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="514"/>
        <source>Paste</source>
        <translation>Incolla</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="515"/>
        <source>Check for Updates</source>
        <translation>Controlla aggiornamenti</translation>
    </message>
    <message>
        <location filename="ui_mainwindow.py" line="516"/>
        <source>Save Debug Log</source>
        <translation>Salva il registro di debug</translation>
    </message>
</context>
<context>
    <name>SettingsDialogBox</name>
    <message>
        <location filename="settings.py" line="67"/>
        <source>Replace:</source>
        <translation>Sostituire:</translation>
    </message>
    <message>
        <location filename="settings.py" line="68"/>
        <source>With:</source>
        <translation>Con:</translation>
    </message>
</context>
<context>
    <name>SpellCheckEdit</name>
    <message>
        <location filename="spellCheckEdit.py" line="123"/>
        <source>Spelling:</source>
        <translation>Spelling:</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="132"/>
        <source>Add to dictionary</source>
        <translation>Aggiungi al dizionario</translation>
    </message>
</context>
<context>
    <name>WriteTypeMain</name>
    <message>
        <location filename="main.py" line="226"/>
        <source>All Compatible Files (*.wtd *.htm *.html *.txt);;WriteType Document (*.wtd);;Formatted Text (*.html *.htm);;All Files (*.*)</source>
        <translation>Tutti i file compatibili (*.wtd *.htm *.html *.txt);;Documento di WriteType (*.wtd);;Testo in formato HTML (*.html *.htm);;Tutti i file (*.*)</translation>
    </message>
    <message>
        <location filename="main.py" line="261"/>
        <source>WriteType Document (*.wtd);;Formatted Text (*.html);;Plain Text (*.txt)</source>
        <translation>Documento di WriteType (*.wtd);;Testo in formato HTML (*.html);;Testo semplice (*.txt)</translation>
    </message>
    <message>
        <location filename="main.py" line="687"/>
        <source>Invalid, no sentences found.</source>
        <translation>Non valido, nessuna frase trovata.</translation>
    </message>
</context>
<context>
    <name>distractionFree</name>
    <message>
        <location filename="ui_distractionfree.py" line="37"/>
        <source>Dialog</source>
        <translation>Finestra di dialogo</translation>
    </message>
</context>
<context>
    <name>settingsDialog</name>
    <message>
        <location filename="ui_settings.py" line="231"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="232"/>
        <source>Please select the size of the word completion database:</source>
        <translation>Seleziona la dimensione della lista di parole per il completamento:</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="233"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Bitstream Vera Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;None available&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Bitstream Vera Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Nessuna disponibile&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="238"/>
        <source>Word lists</source>
        <translation>Liste di parole</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="239"/>
        <source>View settings for the custom word completion</source>
        <translation>Vedi impostazioni per il completamento personalizzato delle parole</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="240"/>
        <source>Minimum letters:</source>
        <translation>Numero minimo di lettere:</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="241"/>
        <source>Offer phrase completions</source>
        <translation>Suggerisci completamenti della frase</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="242"/>
        <source>Try to guess misspellings</source>
        <translation>Gli errori ortografici possono includere le lettere iniziali? (lento)</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="243"/>
        <source>Misspelling Settings</source>
        <translation>Impostazioni di ortografia</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="244"/>
        <source>How many entries need to be displayed, at the minimum, before WriteType will attempt to guess the spelling?</source>
        <translation>Quanti candidati devono essere visualizzato (almeno), prima che WriteType tenti di correggere l&apos;ortografia autonomamente?</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="245"/>
        <source>Misspelling Threshold:</source>
        <translation>Minimo di candidati:</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="246"/>
        <source>Advanced Substitutions?</source>
        <translation>Controllo ortografico esteso?</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="247"/>
        <source>Word Completion</source>
        <translation>Completamento parola</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="248"/>
        <source>Please enter any custom words you would like to appear in the spell check, one per line.</source>
        <translation>Scrivi qui le parole che vuoi far apparire nel riquadro di correzione ortografica (una parola per riga).</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="249"/>
        <source>Custom Words</source>
        <translation>Lista personale di parole</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="250"/>
        <source>Use auto-correction</source>
        <translation>Attiva auto-correzione</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="251"/>
        <source>Auto-correction settings</source>
        <translation>Impostazioni auto-correzione</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="252"/>
        <source>Contractions</source>
        <translation>Elisioni</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="253"/>
        <source>Auto-corrections</source>
        <translation>Auto-correzioni</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="254"/>
        <source>Default Font:</source>
        <translation>Carattere predefinito:</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="255"/>
        <source>Options:</source>
        <translation>Opzioni:</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="256"/>
        <source>Reading speed:</source>
        <translation>Velocità di lettura:</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="257"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="258"/>
        <source>TTS Engine</source>
        <translation>Programma di lettura</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="259"/>
        <source>System Default</source>
        <translation>Carattere predefinito dal sistema</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="260"/>
        <source>Use Boring Interface (requires restart)</source>
        <translation>Usa interfaccia &quot;seriosa&quot; (necessita un riavvio del programma)</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="261"/>
        <source>Read words back as they are typed</source>
        <translation>Leggi le parole appena vengono scritte</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="262"/>
        <source>Check document for grammar mistakes</source>
        <translation>Esegui controllo grammaticale del documento</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="263"/>
        <source>Check document for spelling mistakes</source>
        <translation>Esegui controllo ortografico</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="264"/>
        <source>Other</source>
        <translation>Altro</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="265"/>
        <source>Okay</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="266"/>
        <source>Apply</source>
        <translation>Applica</translation>
    </message>
    <message>
        <location filename="ui_settings.py" line="267"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
</context>
<context>
    <name>statisticsDialog</name>
    <message>
        <location filename="ui_statistics.py" line="79"/>
        <source>Dialog</source>
        <translation>Finestra di dialogo</translation>
    </message>
    <message>
        <location filename="ui_statistics.py" line="80"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Document Statistics&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Statistiche sul documento&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="ui_statistics.py" line="85"/>
        <source>Filename: </source>
        <translation>Nome del file:</translation>
    </message>
    <message>
        <location filename="ui_statistics.py" line="86"/>
        <source>Characters: </source>
        <translation>Caratteri:</translation>
    </message>
    <message>
        <location filename="ui_statistics.py" line="87"/>
        <source>Sentences: </source>
        <translation>Frasi:</translation>
    </message>
    <message>
        <location filename="ui_statistics.py" line="88"/>
        <source>Paragraphs: </source>
        <translation>Paragrafi:</translation>
    </message>
    <message>
        <location filename="ui_statistics.py" line="89"/>
        <source>Readability</source>
        <translation>Leggibilità</translation>
    </message>
    <message>
        <location filename="ui_statistics.py" line="90"/>
        <source>Words: </source>
        <translation>Parole:</translation>
    </message>
</context>
</TS>
