<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="ru_RU">
<context>
    <name>EspeakInferface</name>
    <message>
        <location filename="espeakInterface.py" line="56"/>
        <source>Feature unavailable</source>
        <translation>Возможность недоступна</translation>
    </message>
</context>
<context>
    <name>EspeakInterface</name>
    <message>
        <location filename="espeakInterface.py" line="56"/>
        <source>eSpeak is not installed on this computer.  To use this feature, please install eSpeak or select a new TTS driver in the Settings box.</source>
        <translation>eSpeak не установлен на этом компьютере.  Для использования этой возможности установите eSpeak или выберите новый драйвер TTS в диалоге Настройки.</translation>
    </message>
</context>
<context>
    <name>FestivalInterface</name>
    <message>
        <location filename="festivalInterface.py" line="60"/>
        <source>Feature unavailable</source>
        <translation>Возможность недоступна</translation>
    </message>
    <message>
        <location filename="festivalInterface.py" line="60"/>
        <source>Festival is not installed on this computer.  To use this feature, please install Festival or select a new TTS driver in the Settings box.</source>
        <translation>Festival не установлен на этом компьютере.  Для использования этой возможности установите Festival или выберите новый драйвер TTS в диалоге Настройки.</translation>
    </message>
</context>
<context>
    <name>MainApplication</name>
    <message>
        <location filename="main.py" line="719"/>
        <source>WriteType - </source>
        <translation>WriteType - </translation>
    </message>
    <message>
        <location filename="main.py" line="152"/>
        <source>Untitled Document</source>
        <translation>Документ без названия</translation>
    </message>
    <message>
        <location filename="main.py" line="202"/>
        <source>Crash recovery</source>
        <translation>Восстановление после краха</translation>
    </message>
    <message>
        <location filename="main.py" line="202"/>
        <source>WriteType found unsaved work from a crash.  Would you like to recover it?</source>
        <translation>WriteType нашёл несохранённую работу после краха приложения.  Хотите восстановить её?</translation>
    </message>
    <message>
        <location filename="main.py" line="206"/>
        <source>Recovered file</source>
        <translation>Восстановленный файл</translation>
    </message>
    <message>
        <location filename="main.py" line="226"/>
        <source>Open file</source>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="main.py" line="622"/>
        <source>Save file</source>
        <translation>Сохранить файл</translation>
    </message>
    <message>
        <location filename="main.py" line="634"/>
        <source>Save error</source>
        <translation>Ошибка сохранения</translation>
    </message>
    <message>
        <location filename="main.py" line="295"/>
        <source>WriteType was unable to save your work.  Please check the file extension, ensure that the selected file is writable, and try again.</source>
        <translation>WriteType не удалось сохранить вашу работу.  Проверьте расширения файла, убедитесь, что выбранный файл доступен для записи, и попробуйте снова.</translation>
    </message>
    <message>
        <location filename="main.py" line="326"/>
        <source>Feature unavailable</source>
        <translation>Возможность недоступна</translation>
    </message>
    <message>
        <location filename="main.py" line="326"/>
        <source>The current TTS driver is invalid.  Read-back is unavailable for this session.</source>
        <translation>Текущий драйвер TTS некорректен.  Прочтение недоступно в этой сессии.</translation>
    </message>
    <message>
        <location filename="main.py" line="564"/>
        <source>&lt;i&gt;No suggestion available.&lt;/i&gt;</source>
        <translation>&lt;i&gt;Нет доступных вариантов.&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="573"/>
        <source>&lt;i&gt;Diction check completed.&lt;/i&gt;</source>
        <translation>&lt;i&gt;Словарная проверка завершена.&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="601"/>
        <source>About this program</source>
        <translation>Об этой программе</translation>
    </message>
    <message>
        <location filename="main.py" line="590"/>
        <source>&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;Revision r%1&lt;/span&gt;&lt;/h1&gt;&lt;h2&gt;Copyright 2010 Max Shinn&lt;/h2&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;This software is made available under the GNU General Public License v3 or later. For more information about your rights, see: &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Additional Contributions&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Spanish Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Basque Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Dutch Translations&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</source>
        <translation type="obsolete">&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;ревизия r%1&lt;/span&gt;&lt;/h1&gt;&lt;h2&gt;Авторское право © 2010 Max Shinn&lt;/h2&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;Это программное обеспечение доступно по лицензии GNU General Public License версии 3 или более поздней. Больше о ваших правах вы можете узнать на &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Другие участники&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Испанский перевод&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Баскский перевод&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Голландский перевод&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Sergey Basalaev&lt;/td&gt;&lt;td&gt;Русский перевод&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="609"/>
        <source>&lt;html&gt;A new version of WriteType is available!  You are using WriteType version r%2.  Find more information about WriteType version r%3 at: &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;Доступна новая версия WriteType!  Вы используете WriteType версии r%2.  Больше информации о WriteType версии r%3 можно найти на сайте: &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="611"/>
        <source>Your version of WriteType is up to date.  You are using WriteType version r%1.</source>
        <translation>У вас самая последняя версия WriteType.  Вы используете WriteType версии r%1.</translation>
    </message>
    <message>
        <location filename="main.py" line="613"/>
        <source>There was an unexpected error in establishing a connection.  Please try again later.</source>
        <translation>При соединении произошла неожиданная ошибка.  Попробуйте позднее.</translation>
    </message>
    <message>
        <location filename="main.py" line="614"/>
        <source>Updates</source>
        <translation>Обновления</translation>
    </message>
    <message>
        <location filename="main.py" line="635"/>
        <source>WriteType was unable to save the log file.  Please check the file extension, ensure that the selected file is writable, and try again.</source>
        <translation>WriteType не удалось сохранить файл журнала.  Проверьте расширение файла, убедитесь, что выбранный файл доступен для записи, и попробуйте снова.</translation>
    </message>
    <message>
        <location filename="main.py" line="645"/>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <location filename="main.py" line="731"/>
        <source>Quit?</source>
        <translation>Выйти?</translation>
    </message>
    <message>
        <location filename="main.py" line="731"/>
        <source>You have unsaved work.  Do you want to save?</source>
        <translation>У вас есть несохранённая работа.  Хотите сохранить?</translation>
    </message>
    <message>
        <location filename="main.py" line="601"/>
        <source>&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;Revision r%1&lt;/span&gt;&lt;/h1&gt;&lt;h2&gt;Copyright 2010 Max Shinn&lt;/h2&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;This software is made available under the GNU General Public License v3 or later. For more information about your rights, see: &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Additional Contributions&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Spanish Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Basque Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Dutch Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:sbasalaev@gmail.com&quot;&gt;Sergey Basalaev&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Russian Translations&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</source>
        <translation>&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;ревизия r%1&lt;/span&gt;&lt;/h1&gt;&lt;h2&gt;Авторское право © 2010 Max Shinn&lt;/h2&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;Это программное обеспечение доступно по лицензии GNU General Public License версии 3 или более поздней. Больше о ваших правах вы можете узнать на &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Другие участники&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Испанский перевод&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Баскский перевод&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Голландский перевод&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:sbasalaev@gmail.com&quot;&gt;Sergey Basalaev&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Русский перевод&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.py" line="444"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="445"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="446"/>
        <source>Next</source>
        <translation>Дальше</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="447"/>
        <source>Go Back </source>
        <translation>Назад </translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="448"/>
        <source>View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="449"/>
        <source>Toolbars</source>
        <translation>Панели управления</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="454"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="451"/>
        <source>Tools</source>
        <translation>Инструменты</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="452"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="455"/>
        <source>Edit</source>
        <translation>Правка</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="456"/>
        <source>Speak text</source>
        <translation>Произнести текст</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="457"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="458"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="459"/>
        <source>Save As...</source>
        <translation>Сохранить как...</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="460"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="461"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="462"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="463"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="465"/>
        <source>Bold</source>
        <translation>Полужирный</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="466"/>
        <source>Ctrl+B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="468"/>
        <source>Italic</source>
        <translation>Курсив</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="469"/>
        <source>Ctrl+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="471"/>
        <source>Underline</source>
        <translation>Подчёркнутый</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="472"/>
        <source>Ctrl+U</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="473"/>
        <source>Speak</source>
        <translation>Произнести</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="474"/>
        <source>Speak Text</source>
        <translation>Произнести текст</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="476"/>
        <source>Undo</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="477"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="479"/>
        <source>Redo</source>
        <translation>Повторить</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="480"/>
        <source>Ctrl+Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="481"/>
        <source>File Toolbar</source>
        <translation>Панель «Файл»</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="482"/>
        <source>Enable File Toolbar</source>
        <translation>Отображать панель управления «Файл»</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="483"/>
        <source>Edit Toolbar</source>
        <translation>Панель управления «Правка»</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="484"/>
        <source>Enable Edit Toolbar</source>
        <translation>Отображать панель управления «Правка»</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="485"/>
        <source>Speaker Toolbar</source>
        <translation>Панель «Прочтение»</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="486"/>
        <source>Enable Speaker Toolbar</source>
        <translation>Отображать панель управления «Прочтение»</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="487"/>
        <source>Documentation</source>
        <translation>Документация</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="489"/>
        <source>About WriteType</source>
        <translation>О WriteType</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="490"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="492"/>
        <source>Print</source>
        <translation>Печать</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="493"/>
        <source>Ctrl+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="495"/>
        <source>Highlight Mode</source>
        <translation>Режим подсветки</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="497"/>
        <source>Highlight</source>
        <translation>Подсветка</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="498"/>
        <source>Distraction Free</source>
        <translation>На весь экран</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="499"/>
        <source>Distraction Free Mode</source>
        <translation>Полноэкранный режим</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="500"/>
        <source>Insert Image</source>
        <translation>Вставить рисунок</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="501"/>
        <source>Align Image Left</source>
        <translation>Выровнять рисунок по левому краю</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="502"/>
        <source>Align Image Right</source>
        <translation>Выровнять рисунок по правому краю</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="503"/>
        <source>Image Toolbar</source>
        <translation>Панель «Рисунок»</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="504"/>
        <source>Stop</source>
        <translation>Остановить</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="505"/>
        <source>Align Left</source>
        <translation>Выровнять по левому краю</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="506"/>
        <source>Align Center</source>
        <translation>Выровнять по центру</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="507"/>
        <source>Align Right</source>
        <translation>Выровнять по правому краю</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="508"/>
        <source>Double Space</source>
        <translation>Двойной пробел</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="509"/>
        <source>Single Space</source>
        <translation>Простой пробел</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="510"/>
        <source>Statistics</source>
        <translation>Статистика</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="511"/>
        <source>Diction Check</source>
        <translation>Словарная проверка</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="512"/>
        <source>Cut</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="513"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="514"/>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="515"/>
        <source>Check for Updates</source>
        <translation>Проверить обновления</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="516"/>
        <source>Save Debug Log</source>
        <translation>Сохранить журнал отладки</translation>
    </message>
</context>
<context>
    <name>SettingsDialogBox</name>
    <message>
        <location filename="settings.py" line="67"/>
        <source>Replace:</source>
        <translation>Заменить:</translation>
    </message>
    <message>
        <location filename="settings.py" line="68"/>
        <source>With:</source>
        <translation>На:</translation>
    </message>
</context>
<context>
    <name>SpellCheckEdit</name>
    <message>
        <location filename="spellCheckEdit.py" line="123"/>
        <source>Spelling:</source>
        <translation>Орфография:</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="132"/>
        <source>Add to dictionary</source>
        <translation>Добавить в словарь</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="390"/>
        <source>Sentence starts without a capital letter</source>
        <translation type="obsolete">Предложение начинается не с заглавной буквы</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="394"/>
        <source>No space after punctuation</source>
        <translation type="obsolete">Нет пробела после знака препинания</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="398"/>
        <source>Too many spaces</source>
        <translation type="obsolete">Слишком много пробелов</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="402"/>
        <source>Spaces before punctuation</source>
        <translation type="obsolete">Пробелы перед знаком препинания</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="410"/>
        <source>Use &apos;an&apos; instead of &apos;a&apos;</source>
        <translation type="obsolete">Используйте &apos;an&apos; вместо &apos;a&apos;</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="418"/>
        <source>Use &apos;a&apos; instead of &apos;an&apos;</source>
        <translation type="obsolete">Используйте &apos;a&apos; вместо &apos;an&apos;</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="422"/>
        <source>Word repeated</source>
        <translation type="obsolete">Слово повторяется</translation>
    </message>
</context>
<context>
    <name>WriteTypeMain</name>
    <message>
        <location filename="main.py" line="682"/>
        <source>Invalid, no sentences found.</source>
        <translation>Некорректно, не найдено предложений.</translation>
    </message>
    <message>
        <location filename="main.py" line="226"/>
        <source>All Compatible Files (*.wtd *.htm *.html *.txt);;WriteType Document (*.wtd);;Formatted Text (*.html *.htm);;All Files (*.*)</source>
        <translation>Все совместимые файлы (*.wtd *.htm *.html *.txt);;Документы WriteType (*.wtd);;Форматированный текст (*.html *.htm);;Все файлы (*.*)</translation>
    </message>
    <message>
        <location filename="main.py" line="259"/>
        <source>WriteType Document (*.wtd);;Formatted Text (*.html);;Plain Text (*.txt)</source>
        <translation>Документ WriteType (*.wtd);;Форматированный текст (*.html);;Простой текст (*.txt)</translation>
    </message>
</context>
<context>
    <name>distractionFree</name>
    <message>
        <location filename="distractionFree.py" line="37"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
</context>
<context>
    <name>settingsDialog</name>
    <message>
        <location filename="settingsDialog.py" line="231"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="248"/>
        <source>Please enter any custom words you would like to appear in the spell check, one per line.</source>
        <translation>Введите слова, которые вы хотите добавить к проверке орфографии, по одному в строке.</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="249"/>
        <source>Custom Words</source>
        <translation>Пользовательские слова</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="232"/>
        <source>Please select the size of the word completion database:</source>
        <translation>Выберите размер базы данных автодополнения слов:</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="238"/>
        <source>Word lists</source>
        <translation>Списки слов</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="239"/>
        <source>View settings for the custom word completion</source>
        <translation>Просмотр настроек автодополнения пользовательских слов</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="240"/>
        <source>Minimum letters:</source>
        <translation>Минимум букв:</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="241"/>
        <source>Offer phrase completions</source>
        <translation>Предлагать автодополнение фраз</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="242"/>
        <source>Try to guess misspellings</source>
        <translation>Пытаться угадывать опечатки</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="243"/>
        <source>Misspelling Settings</source>
        <translation>Настройки опечаток</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="244"/>
        <source>How many entries need to be displayed, at the minimum, before WriteType will attempt to guess the spelling?</source>
        <translation>Какое минимальное число элементов должно быть отображено прежде, чем WriteType попытается угадать написание?</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="245"/>
        <source>Misspelling Threshold:</source>
        <translation>Порог опечатки:</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="246"/>
        <source>Advanced Substitutions?</source>
        <translation>Расширенные подстановки?</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="247"/>
        <source>Word Completion</source>
        <translation>Автодополнение слов</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="250"/>
        <source>Use auto-correction</source>
        <translation>Использовать автокоррекцию</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="251"/>
        <source>Auto-correction settings</source>
        <translation>Настройки автокоррекции</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="252"/>
        <source>Contractions</source>
        <translation>Сокращения</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="253"/>
        <source>Auto-corrections</source>
        <translation>Авто-коррекция</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="254"/>
        <source>Default Font:</source>
        <translation>Шрифт по умолчанию:</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="255"/>
        <source>Options:</source>
        <translation>Опции:</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="256"/>
        <source>Reading speed:</source>
        <translation>Скорость чтения:</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="257"/>
        <source>%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="258"/>
        <source>TTS Engine</source>
        <translation>Движок TTS</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="259"/>
        <source>System Default</source>
        <translation>Системный</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="262"/>
        <source>Check document for grammar mistakes</source>
        <translation>Проверить документ на грамматические ошибки</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="260"/>
        <source>Use Boring Interface (requires restart)</source>
        <translation>Использовать скучный интерфейс (необходим перезапуск)</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="261"/>
        <source>Read words back as they are typed</source>
        <translation>Произносить слова по мере печати</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="264"/>
        <source>Other</source>
        <translation>Другое</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="265"/>
        <source>Okay</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="266"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="267"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="233"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Bitstream Vera Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;None available&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="263"/>
        <source>Check document for spelling mistakes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>statisticsDialog</name>
    <message>
        <location filename="statistics.py" line="79"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="statistics.py" line="80"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Document Statistics&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Статистика документа&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="statistics.py" line="85"/>
        <source>Filename: </source>
        <translation>Имя файла: </translation>
    </message>
    <message>
        <location filename="statistics.py" line="86"/>
        <source>Characters: </source>
        <translation>Символы: </translation>
    </message>
    <message>
        <location filename="statistics.py" line="87"/>
        <source>Sentences: </source>
        <translation>Предложения: </translation>
    </message>
    <message>
        <location filename="statistics.py" line="88"/>
        <source>Paragraphs: </source>
        <translation>Абзацы: </translation>
    </message>
    <message>
        <location filename="statistics.py" line="89"/>
        <source>Readability</source>
        <translation>Читаемость</translation>
    </message>
    <message>
        <location filename="statistics.py" line="90"/>
        <source>Words: </source>
        <translation>Слова: </translation>
    </message>
</context>
</TS>
