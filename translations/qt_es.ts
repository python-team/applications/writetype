<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1">
<context>
    <name>EspeakInferface</name>
    <message>
        <location filename="espeakInterface.py" line="56"/>
        <source>Feature unavailable</source>
        <translation type="unfinished">Característica no disponible</translation>
    </message>
</context>
<context>
    <name>EspeakInterface</name>
    <message>
        <location filename="espeakInterface.py" line="56"/>
        <source>eSpeak is not installed on this computer.  To use this feature, please install eSpeak or select a new TTS driver in the Settings box.</source>
        <translation>eSpeak no está instalado en el equipo.  Para usar esta característica, por favor instale eSpeak o seleccione un nuevo controlador de texto a voz en la ventana de Opciones.</translation>
    </message>
</context>
<context>
    <name>FestivalInterface</name>
    <message>
        <location filename="festivalInterface.py" line="60"/>
        <source>Feature unavailable</source>
        <translation>Característica no disponible</translation>
    </message>
    <message>
        <location filename="festivalInterface.py" line="60"/>
        <source>Festival is not installed on this computer.  To use this feature, please install Festival or select a new TTS driver in the Settings box.</source>
        <translation>Festival no está instalado en el equipo.  Para usar esta característica, por favor instale Festival o seleccione un nuevo controlador de texto a voz en la ventana de Opciones.</translation>
    </message>
</context>
<context>
    <name>MainApplication</name>
    <message>
        <location filename="main.py" line="719"/>
        <source>WriteType - </source>
        <translation>WriteType - </translation>
    </message>
    <message>
        <location filename="main.py" line="152"/>
        <source>Untitled Document</source>
        <translation>Documento sin Título</translation>
    </message>
    <message>
        <location filename="main.py" line="202"/>
        <source>Crash recovery</source>
        <translation>Recuperación</translation>
    </message>
    <message>
        <location filename="main.py" line="202"/>
        <source>WriteType found unsaved work from a crash.  Would you like to recover it?</source>
        <translation>WriteType encontró trabajo sin guardar de una sesión cerrada incorrectamente. ¿Desea recuperarlo?</translation>
    </message>
    <message>
        <location filename="main.py" line="206"/>
        <source>Recovered file</source>
        <translation>Archivo recuperado</translation>
    </message>
    <message>
        <location filename="main.py" line="226"/>
        <source>Open file</source>
        <translation>Abrir archivo</translation>
    </message>
    <message>
        <location filename="main.py" line="622"/>
        <source>Save file</source>
        <translation>Guardar archivo</translation>
    </message>
    <message>
        <location filename="main.py" line="326"/>
        <source>Feature unavailable</source>
        <translation>Característica no disponible</translation>
    </message>
    <message>
        <location filename="main.py" line="326"/>
        <source>The current TTS driver is invalid.  Read-back is unavailable for this session.</source>
        <translation>El controlador de texto a voz actual no es válido. La lectura no estará disponible en esta sesión.</translation>
    </message>
    <message>
        <location filename="main.py" line="601"/>
        <source>About this program</source>
        <translation>Sobre este programa</translation>
    </message>
    <message>
        <location filename="main.py" line="645"/>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="main.py" line="731"/>
        <source>Quit?</source>
        <translation>¿Salir?</translation>
    </message>
    <message>
        <location filename="main.py" line="731"/>
        <source>You have unsaved work.  Do you want to save?</source>
        <translation>Tienes trabajo sin guardar. ¿Deseas guardarlo?</translation>
    </message>
    <message>
        <location filename="main.py" line="564"/>
        <source>&lt;i&gt;No suggestion available.&lt;/i&gt;</source>
        <translation>&lt;i&gt;No hay sugerencias disponibles.&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="573"/>
        <source>&lt;i&gt;Diction check completed.&lt;/i&gt;</source>
        <translation>&lt;i&gt;La revisión de la dicción se ha completado.&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="634"/>
        <source>Save error</source>
        <translation>Error al guardar</translation>
    </message>
    <message>
        <location filename="main.py" line="295"/>
        <source>WriteType was unable to save your work.  Please check the file extension, ensure that the selected file is writable, and try again.</source>
        <translation>WriteType no ha podido guardar su trabajo. Por favor revise la extensión del archivo asegúrese de que el archivo pueda ser escrito, y pruebe de nuevo.</translation>
    </message>
    <message>
        <location filename="main.py" line="590"/>
        <source>&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;Revision r%1&lt;/span&gt;&lt;/h1&gt;&lt;h2&gt;Copyright 2010 Max Shinn&lt;/h2&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;This software is made available under the GNU General Public License v3 or later. For more information about your rights, see: &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Additional Contributions&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Spanish Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Basque Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Dutch Translations&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</source>
        <translation type="obsolete">&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;Revisión r%1&lt;/span&gt;&lt;/h1&gt;&lt;h2&gt;Copyright 2010 Max Shinn&lt;/h2&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;Este programa está disponible bajo la licencia GNU GPL v3 o posterior. Para más información sobre sus derechos, vea: &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Contribuciones Adicionales&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Traducción al Español&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Traducción al Euskera&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Traducción al Neerlandés&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="609"/>
        <source>&lt;html&gt;A new version of WriteType is available!  You are using WriteType version r%2.  Find more information about WriteType version r%3 at: &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;¡Una nueva versión de WriteType está disponible!  Actualmente usted está utilizando WriteType versión r%2.  Puede conocer más sobre la nueva versión r%3 de WriteType en: &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="611"/>
        <source>Your version of WriteType is up to date.  You are using WriteType version r%1.</source>
        <translation>Su versión de WriteType está actualizada. Está usando WriteType versión r%1.</translation>
    </message>
    <message>
        <location filename="main.py" line="613"/>
        <source>There was an unexpected error in establishing a connection.  Please try again later.</source>
        <translation>Ocurrió un error inesperado al establecer una conexión. Por favor, inténtelo más tarde.</translation>
    </message>
    <message>
        <location filename="main.py" line="614"/>
        <source>Updates</source>
        <translation>Actualizaciones</translation>
    </message>
    <message>
        <location filename="main.py" line="635"/>
        <source>WriteType was unable to save the log file.  Please check the file extension, ensure that the selected file is writable, and try again.</source>
        <translation>WriteType no ha podido guardar el archivo de registro. Por favor, revise la extensión del archivo, asegúrese que el archivo seleccionado puede ser escrito, y pruebe de nuevo.</translation>
    </message>
    <message>
        <location filename="main.py" line="601"/>
        <source>&lt;h1&gt;WriteType &lt;span style=&quot;font-size: large&quot;&gt;Revision r%1&lt;/span&gt;&lt;/h1&gt;&lt;h2&gt;Copyright 2010 Max Shinn&lt;/h2&gt;&lt;br /&gt;&lt;a href=&quot;mailto:admin@bernsteinforpresident.com&quot;&gt;admin@BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;&lt;a href=&quot;http://bernsteinforpresident.com&quot;&gt;http://BernsteinForPresident.com&lt;/a&gt; &lt;br /&gt;This software is made available under the GNU General Public License v3 or later. For more information about your rights, see: &lt;a href=&quot;http://www.gnu.org/licenses/gpl.html&quot;&gt;http://www.gnu.org/licenses/gpl.html&lt;/a&gt;&lt;br /&gt;&lt;h3&gt;Additional Contributions&lt;/h3&gt;&lt;table border=&quot;1&quot; width=&quot;100%&quot;&gt;&lt;tr&gt;&lt;td&gt;Emilio Lopez&lt;/td&gt;&lt;td&gt;Spanish Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Gorka Azkarate&lt;/td&gt;&lt;td&gt;Basque Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Harm Bathoorn&lt;/td&gt;&lt;td&gt;Dutch Translations&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;a href=&quot;mailto:sbasalaev@gmail.com&quot;&gt;Sergey Basalaev&lt;/a&gt;&lt;/td&gt;&lt;td&gt;Russian Translations&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.py" line="444"/>
        <source>MainWindow</source>
        <translation>MainWindow</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="447"/>
        <source>Go Back </source>
        <translation>Atrás</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="455"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="448"/>
        <source>View</source>
        <translation>Ver</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="449"/>
        <source>Toolbars</source>
        <translation>Barras de Herramientas</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="452"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="454"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="457"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="458"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="459"/>
        <source>Save As...</source>
        <translation>Guardar Como...</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="460"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="461"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="462"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="463"/>
        <source>Settings</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="465"/>
        <source>Bold</source>
        <translation>Negrita</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="466"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="468"/>
        <source>Italic</source>
        <translation>Itálica</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="469"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="471"/>
        <source>Underline</source>
        <translation>Subrayado</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="472"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="473"/>
        <source>Speak</source>
        <translation>Leer</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="474"/>
        <source>Speak Text</source>
        <translation>Leer Texto</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="476"/>
        <source>Undo</source>
        <translation>Deshacer</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="477"/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="479"/>
        <source>Redo</source>
        <translation>Rehacer</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="480"/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="481"/>
        <source>File Toolbar</source>
        <translation>Barra de Herramientas de Archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="482"/>
        <source>Enable File Toolbar</source>
        <translation>Habilitar Barra de Herramientas de Archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="483"/>
        <source>Edit Toolbar</source>
        <translation>Barra de Herramientas de Edición</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="484"/>
        <source>Enable Edit Toolbar</source>
        <translation>Habilitar Barra de Herramientas de Edición</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="485"/>
        <source>Speaker Toolbar</source>
        <translation>Barra de Herramientas de Lectura</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="486"/>
        <source>Enable Speaker Toolbar</source>
        <translation>Habilitar Barra de Herramientas de Lectura</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="487"/>
        <source>Documentation</source>
        <translation>Documentación</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="489"/>
        <source>About WriteType</source>
        <translation>Sobre WriteType</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="490"/>
        <source>About Qt</source>
        <translation>Sobre Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="492"/>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="493"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="495"/>
        <source>Highlight Mode</source>
        <translation>Modo de Resaltado</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="497"/>
        <source>Highlight</source>
        <translation>Resaltar</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="498"/>
        <source>Distraction Free</source>
        <translation>Libre de Distracciones</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="499"/>
        <source>Distraction Free Mode</source>
        <translation>Modo Libre de Distracciones</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="500"/>
        <source>Insert Image</source>
        <translation>Insertar Imagen</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="501"/>
        <source>Align Image Left</source>
        <translation>Alinear Imagen a la Izquierda</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="502"/>
        <source>Align Image Right</source>
        <translation>Alinear Imagen a la Derecha</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="503"/>
        <source>Image Toolbar</source>
        <translation>Barra de Imágenes</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="504"/>
        <source>Stop</source>
        <translation>Detener</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="505"/>
        <source>Align Left</source>
        <translation>Alinear a la Izquierda</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="506"/>
        <source>Align Center</source>
        <translation>Centrar</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="507"/>
        <source>Align Right</source>
        <translation>Alinear a la Derecha</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="508"/>
        <source>Double Space</source>
        <translation>Espacio Doble</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="509"/>
        <source>Single Space</source>
        <translation>Espacio Simple</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="510"/>
        <source>Statistics</source>
        <translation>Estadísticas</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="446"/>
        <source>Next</source>
        <translation>Siguiente</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="451"/>
        <source>Tools</source>
        <translation>Herramientas</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="511"/>
        <source>Diction Check</source>
        <translation>Revisión de la dicción</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="445"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="512"/>
        <source>Cut</source>
        <translation>Cortar</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="513"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="514"/>
        <source>Paste</source>
        <translation>Pegar</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="456"/>
        <source>Speak text</source>
        <translation>Leer texto</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="515"/>
        <source>Check for Updates</source>
        <translation>Buscar Actualizaciones</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="516"/>
        <source>Save Debug Log</source>
        <translation>Guardar Información de Diagnóstico</translation>
    </message>
</context>
<context>
    <name>SettingsDialogBox</name>
    <message>
        <location filename="settings.py" line="67"/>
        <source>Replace:</source>
        <translation>Reemplazar:</translation>
    </message>
    <message>
        <location filename="settings.py" line="68"/>
        <source>With:</source>
        <translation>Con:</translation>
    </message>
</context>
<context>
    <name>SpellCheckEdit</name>
    <message>
        <location filename="spellCheckEdit.py" line="123"/>
        <source>Spelling:</source>
        <translation>Sugerencias:</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="132"/>
        <source>Add to dictionary</source>
        <translation>Añadir al diccionario</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="390"/>
        <source>Sentence starts without a capital letter</source>
        <translation type="obsolete">La frase no empieza por una letra mayúscula</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="394"/>
        <source>No space after punctuation</source>
        <translation type="obsolete">Falta un espacio tras el signo de puntuación</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="398"/>
        <source>Too many spaces</source>
        <translation type="obsolete">Hay demasiados espacios</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="402"/>
        <source>Spaces before punctuation</source>
        <translation type="obsolete">Hay espacios antes del signo de puntuación</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="410"/>
        <source>Use &apos;an&apos; instead of &apos;a&apos;</source>
        <translation type="obsolete">Usar &apos;an&apos; en vez de &apos;a&apos;</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="418"/>
        <source>Use &apos;a&apos; instead of &apos;an&apos;</source>
        <translation type="obsolete">Usar &apos;a&apos; en vez de &apos;an&apos;</translation>
    </message>
    <message>
        <location filename="spellCheckEdit.py" line="422"/>
        <source>Word repeated</source>
        <translation type="obsolete">La palabra está repetida</translation>
    </message>
</context>
<context>
    <name>WriteTypeMain</name>
    <message>
        <location filename="main.py" line="682"/>
        <source>Invalid, no sentences found.</source>
        <translation>No es válido, no se han encontrado frases completas.</translation>
    </message>
    <message>
        <location filename="main.py" line="226"/>
        <source>All Compatible Files (*.wtd *.htm *.html *.txt);;WriteType Document (*.wtd);;Formatted Text (*.html *.htm);;All Files (*.*)</source>
        <translation>Todos los archivos compatibles (*.wtd *.htm *.html *.txt);;Documentos de WriteType (*.wtd);;Texto con formato (*.html *.htm);;Todos los archivos (*.*)</translation>
    </message>
    <message>
        <location filename="main.py" line="259"/>
        <source>WriteType Document (*.wtd);;Formatted Text (*.html);;Plain Text (*.txt)</source>
        <translation>Documento de WriteType (*.wtd);;Texto con formato (*.html);;Texto plano (*.txt)</translation>
    </message>
</context>
<context>
    <name>distractionFree</name>
    <message>
        <location filename="distractionFree.py" line="37"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
</context>
<context>
    <name>settingsDialog</name>
    <message>
        <location filename="settingsDialog.py" line="231"/>
        <source>Settings</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="248"/>
        <source>Please enter any custom words you would like to appear in the spell check, one per line.</source>
        <translation>Por favor escriba las palabras personalizadas que desearía que aparezcan en la corrección de ortografía, una por línea.</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="249"/>
        <source>Custom Words</source>
        <translation>Palabras Personalizadas</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="232"/>
        <source>Please select the size of the word completion database:</source>
        <translation>Por favor seleccione el tamaño de la base de datos de autocompletado de palabras:</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="238"/>
        <source>Word lists</source>
        <translation>Listas de palabras</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="239"/>
        <source>View settings for the custom word completion</source>
        <translation>Ver opciones de el autocompletado de palabras personalizado</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="240"/>
        <source>Minimum letters:</source>
        <translation>Mínimo de letras:</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="242"/>
        <source>Try to guess misspellings</source>
        <translation>Intentar adivinar errores de ortografía</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="243"/>
        <source>Misspelling Settings</source>
        <translation>Errores de ortografía</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="244"/>
        <source>How many entries need to be displayed, at the minimum, before WriteType will attempt to guess the spelling?</source>
        <translation>¿Cuántas entradas han de ser mostradas, como mínimo, para que WriteType intente adivinar su ortografía?</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="245"/>
        <source>Misspelling Threshold:</source>
        <translation>Límite para Activación:</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="246"/>
        <source>Advanced Substitutions?</source>
        <translation>¿Sustituciones Avanzadas?</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="247"/>
        <source>Word Completion</source>
        <translation>Autocompletado de Palabras</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="251"/>
        <source>Auto-correction settings</source>
        <translation>Opciones de Corrección Automática</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="252"/>
        <source>Contractions</source>
        <translation>Contracciones</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="254"/>
        <source>Default Font:</source>
        <translation>Fuente Predeterminada:</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="255"/>
        <source>Options:</source>
        <translation>Opciones:</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="256"/>
        <source>Reading speed:</source>
        <translation>Velocidad de lectura:</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="257"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="258"/>
        <source>TTS Engine</source>
        <translation>Motor de Texto a Voz</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="259"/>
        <source>System Default</source>
        <translation>Predeterminado del Sistema</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="262"/>
        <source>Check document for grammar mistakes</source>
        <translation>Revisar el documento en busca de errores gramaticales</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="264"/>
        <source>Other</source>
        <translation>Otro</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="265"/>
        <source>Okay</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="266"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="267"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="250"/>
        <source>Use auto-correction</source>
        <translation>Usar correcciones automáticas</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="253"/>
        <source>Auto-corrections</source>
        <translation>Correcciones Automáticas</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="241"/>
        <source>Offer phrase completions</source>
        <translation>Ofrecer autocompletado de frases</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="260"/>
        <source>Use Boring Interface (requires restart)</source>
        <translation>Usar la Interfaz Aburrida (requiere reiniciar)</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="261"/>
        <source>Read words back as they are typed</source>
        <translation>Leer en voz alta las palabras mientras se escriben</translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="233"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Bitstream Vera Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;None available&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="settingsDialog.py" line="263"/>
        <source>Check document for spelling mistakes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>statisticsDialog</name>
    <message>
        <location filename="statistics.py" line="79"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location filename="statistics.py" line="80"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Document Statistics&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;Estadísticas del Documento&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="statistics.py" line="85"/>
        <source>Filename: </source>
        <translation>Nombre de Archivo: </translation>
    </message>
    <message>
        <location filename="statistics.py" line="86"/>
        <source>Characters: </source>
        <translation>Caracteres: </translation>
    </message>
    <message>
        <location filename="statistics.py" line="87"/>
        <source>Sentences: </source>
        <translation>Frases: </translation>
    </message>
    <message>
        <location filename="statistics.py" line="88"/>
        <source>Paragraphs: </source>
        <translation>Párrafos: </translation>
    </message>
    <message>
        <location filename="statistics.py" line="89"/>
        <source>Readability</source>
        <translation>Legibilidad</translation>
    </message>
    <message>
        <location filename="statistics.py" line="90"/>
        <source>Words: </source>
        <translation>Palabras: </translation>
    </message>
</context>
</TS>
